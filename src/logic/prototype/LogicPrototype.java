package logic.prototype;

import java.awt.Point;
import java.util.logging.Level;
import java.util.logging.Logger;
import logic.prototype.common.info.Color;
import logic.prototype.common.info.PieceType;
import logic.prototype.logic.GameState;
import logic.prototype.logic.exceptions.InvalidMoveException;
import logic.prototype.logic.exceptions.InvalidPieceTypeException;

public class LogicPrototype {
    
    static void print(GameState board) {
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++)
                System.out.print(board.getPiece(new Point(j, i)) + " ");
            System.out.println();
        }
    }
    
    public static void main(String[] args) {
      
        Color player = Color.BLACK;
        int[][] intBoard = {
            {0, 1, 0, 1, 0, 1, 0, 1},
            {1, 0, 1, 0, 1, 0, 1, 0},
            {0, 1, 0, 1, 0, 1, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {-1, 0, -1, 0, -1, 0, -1, 0},
            {0, -1, 0, -1, 0, -1, 0, -1},
            {-1, 0, -1, 0, -1, 0, -1, 0},
        };
 
        GameState.Builder builder = new GameState.Builder(intBoard.length);
        
        builder.setCurrentPlayer(player);
        
        for (int y = 0; y < intBoard.length; y++)
            for (int x = 0; x < intBoard.length; x++)
                try {
                    builder.pushPiece(PieceType.getPieceType(intBoard[y][x]));
            } catch (InvalidPieceTypeException e) {
                Logger.getLogger(LogicPrototype.class.getName()).log(Level.SEVERE, null, e);
            }
        
        GameState gameState = builder.result();
        System.out.println(gameState.getSize());
        System.out.println(gameState.getCurrentPlayer());
        print(gameState);
        System.out.println();
        System.out.println(gameState.getPiece(new Point(0, 5)));
        System.out.println(gameState.getPiece(new Point(1, 4)));
        try {
            gameState.move(new Point(0, 5), new Point(1, 4));
            System.out.println();
            print(gameState);
            gameState.move(new Point(1, 2), new Point(2, 3));
            System.out.println();
            print(gameState);
            gameState.move(new Point(2, 5), new Point(3, 4));
            System.out.println();
            print(gameState);
            gameState.move(new Point(2, 3), new Point(0, 5));
            System.out.println();
            print(gameState);
            gameState.move(new Point(3, 4), new Point(2, 3));
            System.out.println();
            print(gameState);
            gameState.move(new Point(3, 2), new Point(1, 4));
            System.out.println();
            print(gameState);
        } catch (InvalidMoveException ex) {
            Logger.getLogger(LogicPrototype.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("You cannot move like that");
            System.exit(-1);
        }
    }
    
}
