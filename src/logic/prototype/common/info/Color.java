package logic.prototype.common.info;

import java.util.Arrays;
import java.util.Comparator;
import logic.prototype.logic.exceptions.InvalidColorException;

public enum Color {
    WHITE (1),
    NONE (0),
    BLACK (-1);
    
    private final int value;
    private static final Color[] sortedValues;
    
    Color(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }
    
    public Color enemy() throws InvalidColorException {
        switch(value) {
            case 1:
                return BLACK;
            case -1:
                return WHITE;
            default:
               throw new InvalidColorException("NONE has no enemies.");
        }
    }
    
    public static Color getColor(int value) throws InvalidColorException {
        try {
            return sortedValues[value + 1];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new InvalidColorException("No such color.");
        }
    }
    
    static {
        sortedValues = values();
        Arrays.sort(sortedValues, new Comparator<Color>() {
            @Override
            public int compare(Color first, Color second) {
                return Integer.compare(first.value, second.value);
            }
        });
    }
}
