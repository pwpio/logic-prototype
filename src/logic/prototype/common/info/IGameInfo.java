package logic.prototype.common.info;

import java.awt.Point;

public interface IGameInfo {
    public Color getCurrentPlayer();
    public int getSize();
    public PieceType getPiece(Point position);
    public boolean isMovable(Point position);
    public boolean[][] getMovablePieces();
    public MoveType validateMove(Point from, Point to);
    public boolean[][] getMoves(Point position);
    public boolean isInBounds(Point position);
}
