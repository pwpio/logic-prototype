package logic.prototype.common.info;

public enum MoveType {
    INVALID,
    WALK,
    JUMP;
}
