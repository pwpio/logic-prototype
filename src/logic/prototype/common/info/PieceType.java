package logic.prototype.common.info;

import java.util.Arrays;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import static logic.prototype.common.info.Color.values;
import logic.prototype.logic.exceptions.InvalidColorException;
import logic.prototype.logic.exceptions.InvalidPieceTypeException;

public enum PieceType {

    NONE (0),
    WHITE_PAWN (1),
    BLACK_PAWN (-1),
    WHITE_QUEEN (2),
    BLACK_QUEEN (-2);
    
    private final int value;
    private static final PieceType[] sortedValues;
    
    PieceType(int value) {
        this.value = value;
    }
    
    public Color getColor() {
        try {
            return Color.getColor((int)Math.signum(value));
        } catch (InvalidColorException e) {
            Logger.getLogger(PieceType.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    public boolean isPawn() {
        return value == 1 || value == -1;
    }
    
    public boolean isQueen() {
        return value == 2 || value == -2;
    }

    @Override
    public String toString() {
        if(value >= 0)
            return " " + value;
        else
            return "" + value;
    }
    
    public static PieceType getPieceType(int value) throws InvalidPieceTypeException {
        try {
            return sortedValues[value + 2];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new InvalidPieceTypeException("No such piece.");
        }
    }
    
    static {
        sortedValues = values();
        Arrays.sort(sortedValues, new Comparator<PieceType>() {
            @Override
            public int compare(PieceType first, PieceType second) {
                return Integer.compare(first.value, second.value);
            }
        });
    }

    public boolean isEnemyOf(PieceType other) {
        try {
            return this.getColor() == other.getColor().enemy();
        } catch (InvalidColorException ex) {
            return false;
        }
    }

    public boolean isAllyOf(PieceType other) {
        return this.getColor() == other.getColor();
    }
}
