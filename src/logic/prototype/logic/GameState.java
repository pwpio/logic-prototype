package logic.prototype.logic;

import logic.prototype.common.info.MoveType;
import logic.prototype.logic.exceptions.InvalidMoveException;
import java.awt.Point;
import logic.prototype.common.info.Color;
import logic.prototype.common.info.IGameInfo;
import logic.prototype.common.info.PieceType;

public class GameState implements IGameInfo {
    
    protected PieceType[][] board;
    protected Color currentPlayer;
    private Point hasToJump;
    
    private boolean forcedJump;
    private boolean[][] movablePieces;
    
    public static class Builder {
        private final PieceType[][] board;
        private Color currentPlayer;
        private Point hasToJump;
        private int index;
        
        public Builder(int size) {
            this.board = new PieceType[size][size];
            this.index = 0;
            this.hasToJump = null;
        }
        
        public void pushPiece(PieceType pieceType) {
            if(index < board.length * board.length) {
                board[index/board.length][index%board.length] = pieceType;
                index++;
            }
        }
        
        public void setCurrentPlayer(Color player) {
            this.currentPlayer = player;
        }
        
        public void setHasToJump(Point position) {
            this.hasToJump = new Point(position);
        }
        
        public GameState result() {
            return new GameState(board, currentPlayer, hasToJump);
        }
    }
    
    private GameState(PieceType[][] board, Color currentPlayer, Point hasToJump) {
        int size = board.length;
        this.board = new PieceType[size][size];
        for (int i = 0; i < board.length; i++)
            System.arraycopy(board[i], 0, this.board[i], 0, board[i].length);

        this.currentPlayer = currentPlayer;
        this.movablePieces = null;
        this.hasToJump = hasToJump;
    }
    
//    public GameState(int size, Color currentPlayer) {
//        this.board = new PieceType[size][size];
//        for (int i = 0; i < size; i++)
//            for (int j = 0; j < size; j++)
//                this.board[i][j] = PieceType.NONE;
//        
//        this.currentPlayer = currentPlayer;
//        this.movablePieces = null;
//        this.hasToJump = null;
//    }

    @Override
    public Color getCurrentPlayer() {
        return currentPlayer;
    }
    
    private void switchPlayer() { 
        if (currentPlayer == Color.WHITE)
            currentPlayer = Color.BLACK;
        else
            currentPlayer = Color.WHITE;
    }
    
    private boolean areAllies(Point first, Point second) {
        return getPiece(first).isAllyOf(getPiece(second));
    }
    
    private boolean areEnemies(Point first, Point second) {
        return getPiece(first).isEnemyOf(getPiece(second));
    }
    
    @Override
    public int getSize() {
        return board.length;
    }

    @Override
    public PieceType getPiece(Point position) {
        return board[position.y][position.x];
    }

    private void setPiece(Point position, PieceType piece) {
        board[position.y][position.x] = piece;
    }
    
    @Override
    public boolean isMovable(Point position) {
        return getMovablePieces()[position.y][position.x]; 
    }

    @Override
    public boolean[][] getMovablePieces() {
        if (movablePieces != null)
            return movablePieces;
        else {
            movablePieces = new boolean[getSize()][getSize()];
            
            if (hasToJump != null) {
                movablePieces[hasToJump.y][hasToJump.x] = true;
                forcedJump = true;
                return movablePieces;
            }
            
            forcedJump = false;
            CHECK_JUMPS:
            for (int i = 0; i < getSize(); i++)
                for (int j = 0; j < getSize(); j++)
                    if (canJump(new Point(j, i))) {
                        forcedJump = true;
                        break CHECK_JUMPS;
                    }
            
            if (forcedJump) 
                for (int i = 0; i < getSize(); i++) 
                    for (int j = 0; j < getSize(); j++)
                        movablePieces[i][j] = canJump(new Point(j, i));
            else 
                for (int i = 0; i < getSize(); i++)
                    for (int j = 0; j < getSize(); j++)
                        movablePieces[i][j] = canWalk(new Point(j, i));
            
            return movablePieces;
        }
    }
    
    private boolean canWalk(Point position) {  
        return validateMoveAssumingMovable(position, new Point(position.x+1, position.y+1)) == MoveType.WALK ||
               validateMoveAssumingMovable(position, new Point(position.x-1, position.y+1)) == MoveType.WALK ||
               validateMoveAssumingMovable(position, new Point(position.x+1, position.y-1)) == MoveType.WALK ||
               validateMoveAssumingMovable(position, new Point(position.x-1, position.y-1)) == MoveType.WALK;
    }
    
    private boolean canJump(Point position) {
        final int[][] increments = {
            {1, 1},
            {1, -1},
            {-1, 1},
            {-1, -1}
        };
        
        if (getPiece(position) == PieceType.NONE || getPiece(position).getColor() != currentPlayer)
            return false;
        else if (getPiece(position).isPawn())
            for (int i = 0; i < 4; i++) {
                Point aux = new Point(
                    position.x + 2*increments[i][0],
                    position.y + 2*increments[i][1]
                );
                
                if (isInBounds(aux) && 
                    validateMoveForPawn(position, aux) == MoveType.JUMP)
                    return true;
            }
        else
            for (int i = 0; i < 4; i++) {
                Point previous = new Point(
                    position.x + increments[i][0],
                    position.y + increments[i][1]
                );
                
                Point current = new Point(
                    position.x + 2*increments[i][0],
                    position.y + 2*increments[i][1]
                );
                
                while (isInBounds(current)) {
                    if (areAllies(previous, position))
                        break;
                    boolean hasEnemy = areEnemies(previous, position);
                    
                    if (hasEnemy && getPiece(current) == PieceType.NONE)
                        return true;
                    
                    previous = current;
                    current.x += increments[i][0];
                    current.y += increments[i][1];
                }     
            }
        
        return false;
    }
    
    private void promote(Point position) {
        if (currentPlayer == Color.WHITE)
            setPiece(position, PieceType.WHITE_QUEEN);
        else setPiece(position, PieceType.BLACK_QUEEN);
    }
    
    public void move(Point from, Point to) throws InvalidMoveException {
        MoveType moveType = validateMove(from, to);
        if (moveType != MoveType.INVALID) {
            setPiece(to, getPiece(from));
            setPiece(from, PieceType.NONE);
            
            boolean promoted = currentPlayer == Color.WHITE ? to.y == getSize() - 1 : to.y == 0;
            if (promoted) 
                promote(to);
                    
            if (moveType == MoveType.WALK)
                switchPlayer();
            else if (moveType == MoveType.JUMP) {
                destroyTheEnemyInYourPath(from, to);

                if (!promoted && canJump(to))
                    hasToJump = to;
                else {
                    switchPlayer();
                    hasToJump = null;
                }
            }
            movablePieces = null;
        }
        else throw new InvalidMoveException("Invalid move.");    
    }

    //assumes only one piece in the path and that it is an enemy
    private void destroyTheEnemyInYourPath(Point from, Point to) {
        int diffY = to.y-from.y;
        int diffX = to.x-from.x;
    
        for (int i = 1; i < Math.abs(diffX); i++) {
            Point aux = new Point(
                from.x + i * (int)Math.signum(diffX),
                from.y + i * (int)Math.signum(diffY)
            );
            
            if (getPiece(aux) != PieceType.NONE) {
                setPiece(aux, PieceType.NONE);
                return;
            }
        }       
    }

    private MoveType validateMoveForPawn(Point from, Point to) {
        int diffY = to.y-from.y;
        int diffX = to.x-from.x;
        
        // destination has to be empty
        if (getPiece(to) == PieceType.NONE) {
            
            // white can move only downwards, black can move only upwards
            if (Math.abs(diffX) == 1 && diffY == currentPlayer.getValue())
                return MoveType.WALK;

            // trying to jump
            if (Math.abs(diffX) == 2 && Math.abs(diffY) == 2) {
                Point aux = new Point(
                        from.x + diffX / 2,
                        from.y + diffY / 2
                );

                // a piece can only jump over an enemy
                if (areEnemies(aux, from))
                    return MoveType.JUMP;
            }
        }   
        return MoveType.INVALID;
    }

    private MoveType validateMoveForQueen(Point from, Point to) {
        int diffY = to.y-from.y;
        int diffX = to.x-from.x;
        
        if (getPiece(to) == PieceType.NONE && 
            Math.abs(diffX) == Math.abs(diffY)) {
            
            int counter = 0;
            for (int i = 1; i < Math.abs(diffX); i++) {
                Point aux = new Point(
                    from.x + i * (int)Math.signum(diffX),
                    from.y + i * (int)Math.signum(diffY)
                );
                
                // cannot jump over friendly piece
                if (areAllies(aux, from))
                    return MoveType.INVALID;
                else if (areEnemies(aux, from))
                    counter++;
            }
            
            // can only move or jump over one enemy
            if (counter == 0)
                return MoveType.WALK;
            if (counter == 1)
                return MoveType.JUMP;
        }
        
        return MoveType.INVALID;
    }
    
    private MoveType validateMoveAssumingMovable(Point from, Point to) {     
        if (!from.equals(to) && isInBounds(to)) {
            if (getPiece(from).isPawn())
                return validateMoveForPawn(from, to);
            else if (getPiece(from).isQueen())
                return validateMoveForQueen(from, to);
        }
        return MoveType.INVALID;
    }
    
    @Override
    public MoveType validateMove(Point from, Point to) {   
        if (isInBounds(from) && isMovable(from)) {
            MoveType moveType = validateMoveAssumingMovable(from, to);
            if (forcedJump && moveType != MoveType.JUMP)
                return MoveType.INVALID;
            else 
                return moveType;
        }
        return MoveType.INVALID;
    }
    
    @Override
    public boolean isInBounds(Point position) {
        return position.x >= 0 && position.x < getSize() && 
               position.y >= 0 && position.y < getSize();
    }

    @Override
    public boolean[][] getMoves(Point position) {
        boolean[][] moves = new boolean[getSize()][getSize()];
        
        for (int i = 0; i < getSize(); i++)
            for (int j = 0; j < getSize(); j++)
                moves[i][j] = validateMove(position, new Point(j, i)) != MoveType.INVALID;
        
        return moves;
    }
    
}