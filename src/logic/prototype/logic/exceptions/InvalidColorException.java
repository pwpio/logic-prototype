package logic.prototype.logic.exceptions;

public class InvalidColorException extends Exception {
    
    public InvalidColorException(String message) {
        super(message);
    }
    
}
