package logic.prototype.logic.exceptions;

public class InvalidPieceTypeException extends Exception{
    
    public InvalidPieceTypeException(String message) {
        super(message);
    }
    
}
